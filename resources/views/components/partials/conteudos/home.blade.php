<div class="row blog-destaques mx-0">
    @foreach($blog as $b)
    <div data-aos="flip-right" class="col-md-4">
        <div class="card card-transparent card-width"> {{--  --}}
            <img onclick="location.href= '{{ route('conteudo', $b->slug)}}';" src="{{url('storage/blog/'.$b->img)}}" class="card-img-top click-link" alt="...">
            <div class="card-body px-0 text-center text-md-left">
                <span class="span-16">{{$b->date}}</span>
                <a href="{{ route('conteudo', $b->slug)}}">
                    <h4 class="text-preto-azulado-get">
                        {!! \Illuminate\Support\Str::limit($b->title, 40, '...')!!}
                    </h4>
                </a>
                <a href="{{ route('conteudo', $b->slug)}}" class="btn btn-outline-dark blog-destaques-btn-spacing btn-smaller">Ler mais</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
