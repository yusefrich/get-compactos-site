<div class="container-fluid background-cinza">
    @php $data = $data->find(2); @endphp
    @if($data)
    <div class="d-none d-md-block">
        <div class="row compactos">
            <div data-aos="fade-right" class="col-spacing-right col-md-5 d-flex justify-content-end">
                <img src="{{url('storage/about/'.$data->img1)}}" alt="">
            </div>
            <div data-aos="fade-right" data-aos-delay="200" class="col-spacing-left col-md-7">
                <p class="text-dark paragraph-20 pt-40" style="max-width: 471px;">
                    {!! $data->subtitle !!}
                </p>
            </div>
        </div>
    </div>

    <div class="compactos d-md-none">
        <div data-aos="fade-right" class=" d-flex justify-content-center">
            <img src="{{url('storage/about/'.$data->img1)}}" alt="">
        </div>
        <div data-aos="fade-right" data-aos-delay="200" class=" ">
            <p class="text-dark paragraph-20 pt-40 text-center mx-auto" style="max-width: 471px;">
                {!! $data->subtitle !!}
            </p>
        </div>
    </div>
    @endif
    <div class="container about-features text-center mt-100 mb-50">
        <div class="row">
            @php $data = $data->find(3); @endphp
            @if($data)
            <div data-aos="flip-right"  class="col-md-4 ">
                <div class="feature-outline mb-20 mb-md-0">
                    <img style="margin-bottom: 14px" src="{{url('storage/about/'.$data->img1)}}" alt="">
                    <p style="max-width: 243px;" class="mr-auto ml-auto">
                        {{$data->title}}
                    </p>
                </div>
            </div>
            @endif
            @php $data = $data->find(4); @endphp
            @if($data)
            <div data-aos="flip-right" data-aos-delay="300" class="col-md-4 ">
                <div class="feature-outline mb-20 mb-md-0">
                    <img style="margin-bottom: 14px" src="{{url('storage/about/'.$data->img1)}}" alt="">
                    <p style="max-width: 237px" class="mr-auto ml-auto">
                        {{$data->title}}
                    </p>
                </div>
            </div>
            @endif
            @php $data = $data->find(5); @endphp
            @if($data)
            <div data-aos="flip-right" data-aos-delay="600" class="col-md-4 ">
                <div class="feature-outline">
                    <img style="margin-bottom: 14px" src="{{url('storage/about/'.$data->img1)}}" alt="">
                    <p style="max-width: 181px;" class="mr-auto ml-auto">
                        {{$data->title}}
                    </p>
                </div>
            </div>
            @endif
        </div>
    </div>
    @php $data = $data->find(6); @endphp
    @if($data)
    <div class="container">
        {{-- <div class="d-none d-md-block"> --}}
            <div class="row compactos-3 pt-md-90">
                <div data-aos="fade-right" style="padding-left: 50px" class="col-spacing-right p-mob-0 col-md-6 d-flex ">
                    <div>
                        <h2 class="text-preto-azulado-get mx-auto mx-md-0 text-center text-md-left" style="max-width: 373px;">
                            {{$data->title}}
                        </h2>
                        <p class="text-dark paragraph-20 text-spacing mx-auto mx-md-0 text-center text-md-left" style="max-width: 360px;">
                            {!! $data->subtitle !!}
                        </p>
                    </div>
                </div>

                <div data-aos="fade-right"  class="col-spacing-left col-md-6 d-none d-md-block">
                    <img class="img-bg-pattern-left" src="{{url('assets_front/imgs/img-bg-pattern.png')}}" alt="">
                    <img  class="img-cover sobre-img1" src="{{url('storage/about/'.$data->img1)}}" alt="">
                    <img data-aos="flip-right" data-aos-delay="300"  class="img-cover sobre-img2" src="{{url('storage/about/'.$data->img2)}}" alt="">
                </div>
                <div class="d-md-none">
                    <div data-aos="fade-up"  data-aos-delay="300" style="margin-bottom: 140px" class="mobile-no-spacing d-flex justify-content-end position-relative mt-20">
                        <img class="img-bg-pattern-left-mobile" src="{{url('assets_front/imgs/img-bg-pattern.png')}}" alt="">
                        <img data-aos="fade-right" class="img-cover ap2-img-mobile " style="min-height: 228px;" src="{{url('storage/about/'.$data->img1)}}" alt="">
                        <img data-aos="fade-right" class="img-cover ap3-img-mobile" src="{{url('storage/about/'.$data->img2)}}" alt="">
                    </div>
                </div>
        
        
            </div>
        {{-- </div> --}}

    </div>
    @endif
    @php $data = $data->find(7); @endphp
    @if($data)
    <div class="d-none d-md-block">
        <div class="row compactos-4 my-200">
            <div data-aos="fade-right"  class="col-spacing-right col-md-6 d-flex justify-content-end">
                <img class="img-bg-pattern-right" src="{{url('assets_front/imgs/img-bg-pattern.png')}}" alt="">
                <img class="img-cover sobre-img3 pl-md-40" src="{{url('storage/about/'.$data->img1)}}" alt="">
            </div>
            <div data-aos="fade-right" data-aos-delay="150" class="col-spacing-left col-md-6">
                <h2 class="text-preto-azulado-get" style="max-width: 407px;">
                    {{$data->title}}
                </h2>
                <p class="text-dark paragraph-20 text-spacing" style="max-width: 390px;">
                    {!! $data->subtitle !!}
                </p>
            </div>
        </div>
    </div>
    <div class="d-md-none">
        <div class="mt-50">
            <div data-aos="fade-right" data-aos-delay="150">
                <h2 class="text-preto-azulado-get text-center mx-auto" style="max-width: 322px;">
                    {{$data->title}}
                </h2>
                <p class="text-dark paragraph-20 text-center mx-auto" style="max-width: 322px;">
                    {!! $data->subtitle !!}
                </p>
            </div>
            <div data-aos="fade-right" class="position-relative mt-50 mb-72">
                <img class="img-bg-pattern-left-mobile" src="{{url('assets_front/imgs/img-bg-pattern.png')}}" alt="">
                <img class="img-cover sobre-img3" style="height: 206px" src="{{url('storage/about/'.$data->img1)}}" alt="">
            </div>
        </div>
    </div>
    @endif
</div>
