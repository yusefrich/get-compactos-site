<div class="text-center mx-4 mx-md-0">
    <img src="{{url('assets_front/imgs/forniture-icon.png')}}" class="pb-2" alt="">
    <h4 class="text-preto-azulado-get">
        Móveis planejados
    </h4>
    <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" id="planejados-desc" style="max-width: 380px;">
        Selecione o projeto de móveis planejados e siga para a próxima etapa 
    </p>
</div>
<div class="d-none d-md-flex justify-content-center mb-40 planned-holder ">
    
</div>
<div class=" mb-40 planned-holder-mobile d-md-none">
    
</div>