<div class="row">
    <div class="col-md-8 col-print-12">
        <div class="text-center ">
            <img src="{{url('assets_front/imgs/payment.svg')}}" class="pb-2" alt="">
            <h4 class="text-preto-azulado-get">
                Quero meu Get
            </h4>
            <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" style="max-width: 380px;">
                Agora que montou o seu compacto dos sonhos, let’s Get a coffee? 
            </p>
        </div>     
        <div data-aos="fade-right" data-aos-delay="600" style="max-width: 555px;" class="mr-auto ml-auto px-4 px-md-0 d-none d-md-block d-print-none">
            {{-- desktop --}}
            {!! Form::open(['route' => 'contact.send', 'class' => 'text-start get-form']) !!}
            <div class="form-group ">
                <label for="input-name text-preto-azulado-get">Nome:</label>
                {{-- <input type="text" class="form-control " id="input-name" name="name" placeholder=""> --}}
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Seu Nome',
                'id'=> 'input-name', 'required']) !!}
            </div>
            <div class="form-group  ">
                <label for="input-email">Email:</label>
                {{-- <input type="email" class="form-control" id="input-email"  name="email" placeholder=""> --}}
                {!! Form::text('mail', null, ['class' => 'form-control','placeholder' =>
                'seu@email.com', 'id'=> 'input-email', 'required']) !!}
            </div>
            <div class="form-group    ">
                <label for="input-phone">Telefone:</label>
                {!! Form::text('phone', null, ['class' => 'form-control ', 'data-mask' => '(00) 00000-0000','placeholder' =>
                '(00) 00000-0000', 'id'=>
                'input-phone', 'required']) !!}
            </div>
            <div class="form-group    mx-0">
                <label for="input-mensagem">Mensagem:</label>
                {!! Form::textarea('msg', null, ['class' => 'form-control ','id'=>
                'input-mensagem', 'required']) !!}
            </div>
            <div class="form-group    mx-0">
                {!! Form::hidden('emp_id', null, ['class' => 'form-control ', 'id'=>
                'input-emp', 'required']) !!}
            </div>
            <div class="form-group    mx-0">
                {!! Form::hidden('payment_sim', null, ['class' => 'form-control ', 'id'=>
                'input-sim', 'required']) !!}
            </div>

            <div class="row pb-72 step-6-form">
                <div class="col-4 pr-1 ">
                    <button data-step-to="step-5" data-step-from="step-6" data-step-dir="prev" class="btn btn-outline-dark btn-block btn-step-move px-0" >Passo Anterior</strong></button>
                </div>
                <div class="col-8  pl-1">
                    <button class="btn btn-dark-get btn-block" type="submit">Enviar proposta</strong></button>
                </div>
            </div>
            </form>
            
            
        </div>   
    </div>
    <div class="col-md-4 p-0 col-print-12">
        <div class="builder-value-result mx-4 mx-md-0">
            <p class="caption-20 text-vermelho-get m-0">Valor do empreendimento</p>
            <h4 class="m-0 final-value-emp">R$ 0,00</h4>
            <p id="final-value-emp-name" class="caption-14 text-vermelho-get m-0 mb-1 mt-3 font-weight-bold">Get one</p>
            <p id="final-value-uni-name" class="m-0 caption-16 text-white">Unidade 1</p>   
            <small id="final-value-uni-price" class="print-price-tag form-text text-white mt-0 final-value-plan">
                6.532,56
            </small>

            <hr>
            <p class="caption-14 text-vermelho-get m-0 mb-1 font-weight-bold">Móveis Planejados</p>
            <p class="m-0 caption-16 text-white final-value-movel-name">Kit 02</p>   
            <small class="form-text text-white mt-0 final-value-plan final-value-movel-price">
                6.532,56
            </small>
            <hr>
            <p class="caption-14 text-vermelho-get  font-weight-bold mb-10">Lista de Eletrodomésticos</p>
            <div class="forni-list"></div>
            <div id="payment-op-print" class="d-none">
                <hr>
                <div style="margin-top: 16px" class="row">
                    <div class="col-12">
                        <p id="final-value-type" class="caption-14 text-vermelho-get m-0 font-weight-bold">tipo 1</p>
                        <small id="final-value-typeDesc" class="form-text text-white mt-0 final-value-plan">
                            desc
                        </small>
                    </div>
                </div>
                <hr>
                <div style="margin-top: 16px" class="row">
                    <div class="col-6 mb-20">
                        <p class="caption-14 text-vermelho-get m-0 font-weight-bold">Entrada</p>
                        <p id="final-value-entrada" class="m-0 caption-16 text-white ">R$ 14.000,00</p>    
                    </div>
                    <div class="col-6 mb-20">
                        <p class="caption-14 text-vermelho-get m-0 font-weight-bold">Semestrais</p>
                        <p id="final-value-semestrais" class="m-0 caption-16 text-white ">R$6.167</p>   
                        <small id="final-value-semestrais-qtd" class="form-text text-white mt-0 final-value-plan">
                            6.532,56
                        </small> 
                    </div>
                    <div class="col-6 mb-20">
                        <p class="caption-14 text-vermelho-get m-0 font-weight-bold">Mensais</p>
                        <p id="final-value-mensais" class="m-0 caption-16 text-white ">R$ 1.200,00</p>    
                        <small id="final-value-mensais-qtd" class="form-text text-white mt-0 final-value-plan">
                            6.532,56
                        </small> 
                    </div>
                    <div class="col-6 mb-20">
                        <p class="caption-14 text-vermelho-get m-0 font-weight-bold">Financiamento</p>
                        <p id="final-value-financiamento" class="m-0 caption-16 text-white ">R$78,067,23</p>    
                    </div>
                </div>
            </div>
            
            <div class="mt-40 d-flex justify-content-center">
                <button onclick="window.print();" class="btn btn-outline-light btn-smaller mt-1 d-print-none"> <i class="icon icon-icon-print print-offset-icon pr-2"></i> Imprimir Resumo</button>
            </div>
        </div>
        <div data-aos="fade-right" data-aos-delay="600" style="max-width: 555px;" class="mr-auto ml-auto px-4 px-md-0 d-md-none d-print-none">
            {{-- mobile --}}
            {!! Form::open(['route' => 'contact.send', 'class' => 'text-start get-form']) !!}
            <div class="form-group ">
                <label for="input-name-mob text-preto-azulado-get">Nome:</label>
                {{-- <input type="text" class="form-control " id="input-name-mob" name="name" placeholder=""> --}}
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Seu Nome',
                'id'=> 'input-name-mob', 'required']) !!}
            </div>
            <div class="form-group  ">
                <label for="input-email-mob">Email:</label>
                {{-- <input type="email" class="form-control" id="input-email"  name="email" placeholder=""> --}}
                {!! Form::text('mail', null, ['class' => 'form-control','placeholder' =>
                'seu@email.com', 'id'=> 'input-email-mob', 'required']) !!}
            </div>
            <div class="form-group    ">
                <label for="input-phone-mob">Telefone:</label>
                {!! Form::text('phone', null, ['class' => 'form-control ', 'data-mask' => '(00) 00000-0000','placeholder' =>
                '(00) 00000-0000', 'id'=>
                'input-phone-mob', 'required']) !!}
            </div>
            <div class="form-group    mx-0">
                <label for="input-mensagem-mob">Mensagem:</label>
                {!! Form::textarea('msg', null, ['class' => 'form-control ','id'=>
                'input-mensagem-mob', 'required']) !!}
            </div>

            <div class="form-group    mx-0">
                {!! Form::hidden('emp_id', null, ['class' => 'form-control ', 'id'=>
                'input-emp-mob', 'required']) !!}
            </div>
            <div class="form-group    mx-0">
                {!! Form::hidden('payment_sim', null, ['class' => 'form-control ', 'id'=>
                'input-sim-mob', 'required']) !!}
            </div>
            <div class="row pb-72 step-6-form">
                <div class="col-6 pr-1 ">
                    <a href="#pageTop" data-step-to="step-5" data-step-from="step-6" data-step-dir="prev" class="btn btn-outline-dark btn-block btn-step-move px-0" >Passo Anterior</strong></a>
                </div>
                <div class="col-6  pl-1">
                    <button class="btn btn-dark-get btn-block px-0" type="submit">Enviar proposta</strong></button>
                </div>
            </div>
            </form>
            
            
        </div>   

    </div>
</div>
