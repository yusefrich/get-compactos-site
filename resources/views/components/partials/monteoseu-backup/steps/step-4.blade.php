@foreach($data as $key => $emp)
@foreach($emp['floors'] as $key => $flor)
@foreach($flor['unities'] as $florkey => $uni)
@foreach($uni as $unikey => $singleUni )
<div id="collapse-electros-{{$flor['id']}}-{{$singleUni['id']}}" class="d-none">
    <div class="text-center mx-4 mx-md-0">
        <img src="{{url('assets_front/imgs/television.svg')}}" class="pb-2" alt="">
        <h4  class="text-preto-azulado-get">
            Lista de eletrodomésticos
        </h4>
        @if($singleUni['electros_new'] != null)
        <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" style="max-width: 380px;">
            Selecione o projeto de móveis planejados e siga para a próxima etapa 
        </p>
        @else
        <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" style="max-width: 380px;">
            Essa unidade não possui Eletrodomésticos cadastrados 
        </p>

        @endif
    </div>
    <div class="container-small mx-auto">
        <div class="row mb-40">
            @if($singleUni['electros_new'] != null)
            @foreach ($singleUni['electros_new'] as $su){{-- {{json_decode($su)->name}} --}}
                <div class="col-md-6 col-right mb-20">
                    <div style="width: 115px" class="custom-control custom-checkbox get-checkbox">
                        <input onclick="setMovel({{json_decode($su)->price}}, '{{json_decode($su)->name}}', this)"style="box-shadow: none" type="checkbox" class="custom-control-input" aria-describedby="movel-value-{{json_decode($su)->id}}" id="forniture-select-check{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">
                        <label class="custom-control-label text-preto-azulado-get" for="forniture-select-check{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">{{json_decode($su)->name}}</label>{{-- {{$singleUni['name']}} --}}
                        <small id="movel-value-{{json_decode($su)->id}}" class="form-text text-vermelho-get">
                            {{'R$ '.number_format(json_decode($su)->price, 2, ',', '.')}}
                        </small>
                    </div>
                </div>
            @endforeach
            @endif
        </div>
    </div>
</div>
@endforeach
@endforeach
@endforeach
@endforeach

