<div class="container-smaller mx-auto background-cinza">
    <div class="container px-0 pt-30">
        <h2 data-aos="fade-right"  style="max-width: 747px" class="text-preto-azulado-get text-center text-md-left">
            Leia nossos conteúdos e fique por dentro do que é Get
        </h2>
        <p data-aos="fade-right" data-aos-delay="300" style="max-width: 660px" class="paragraph-20 text-dark text-center text-md-left">
            Fique informado sobre o mercado imobiliário, arquitetura e urbanismo, confira dicas decoração e muito mais.
        </p>
        
        @include('components.partials.conteudos.home')
        <div class="d-none d-md-block ">
            <div class="d-flex justify-content-center btn-blog-caller-spacing ">
                <a href="{{route('conteudos')}}" data-aos="fade-right" class="btn btn-outline-dark ">Ver Conteúdos</a>
            </div>
        </div>
    </div>
</div>
