
<nav data-aos="fade-down" data-aos-mirror="false" id="menu" class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top bg-transparent container-fluid">
    <div class="container hide-pc">

        <a class="navbar-brand" href="{{ route('home') }}">
            <img id="navbar-brand-img" src="{{ asset('assets_front/imgs/Logo Branca.png') }}" width="auto" height="85" class="d-inline-block align-top nav-brand-img" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                @include('components.partials.component_atoms.navigation')
            </ul>
        </div>
    </div>
    <div class="container d-md-none d-flex justify-content-center">
        <a class="navbar-brand" href="{{ route('home') }}">
            <img id="navbar-brand-img-sm" src="{{ asset('assets_front/imgs/Logo Branca.png') }}" width="auto" height="50" class="d-inline-block align-top nav-brand-img" alt="">
        </a>

        
    </div>
</nav>

<button class=" btn btn-light btn-round p-3 btn-menu-mobile d-md-none showCollapseMenu"  >
    <ion-icon name="menu"></ion-icon>
</button>
<div class="container-fluid mobile-menu menu-hidden menu-hidden-anim" id="menu-mobile-collapse"> {{-- collapse --}}
        <ul class="navbar-nav ml-auto text-right" id="menu-mobile-items">
            <li>
                <a href="{{ route('home') }}">
                    <img style="margin-bottom: 32px" src="{{ asset('assets_front/imgs/Logo Branca.png') }}" width="auto" height="56" class="d-inline-block align-top nav-brand-img" alt="">
                </a>

            </li>
            @include('components.partials.component_atoms.navigation')
            <li style="margin-top: 32px" class="d-flex justify-content-end">
                <a class="btn btn-light btn-round p-2 my-auto mx-10" href="#"><ion-icon style="font-size: 24px;" name="logo-whatsapp"></ion-icon></a>
                <a class="btn btn-light btn-round p-2 my-auto mx-10" href="#"><ion-icon style="font-size: 24px;" name="logo-instagram"></ion-icon></a>
    
                <button style="margin-left: 16px" class=" btn btn-light btn-round p-3 btn-menu-mobile-close d-md-none hideCollapseMenu">
                    <ion-icon name="close"></ion-icon>
                </button>

            </li>
        </ul>

</div>

@push('scripts')
    <!-- navbar top opacity -->
<script>
    $(document).ready(function () {
        var solidMenu = false;

        var setNavbarOpacity = function () {
            var o1 = $("#menu").offset();
            var o2 = $("body").offset();
            var dx = o1.left - o2.left;
            var dy = o1.top - o2.top;
            var distance = Math.sqrt(dx * dx + dy * dy);

            if (distance > 200) {
                jQuery("#menu").removeClass("bg-transparent");
                jQuery("#navbar-brand-img").addClass("brand-small");
                jQuery("#navbar-brand-img-sm").addClass("brand-smaller");

                
                solidMenu = true;
            } else {
                jQuery("#menu").addClass("bg-transparent");
                jQuery("#navbar-brand-img").removeClass("brand-small");
                jQuery("#navbar-brand-img-sm").removeClass("brand-smaller");



                solidMenu = false;

            }
        }

        setNavbarOpacity();

        $(window).scroll(function (event) {
            setNavbarOpacity();
            if(!solidMenu)
                jQuery("#menu").removeClass("hide-menu");

        });


        var lastScrollTop = 0;

        // element should be replaced with the actual target element on which you have applied scroll, use window in case of no target element.
        window.addEventListener("scroll", function(){ // or window.addEventListener("scroll"....
            var st = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
            if (st > lastScrollTop){
                if(solidMenu)
                    jQuery("#menu").addClass("hide-menu");
            } else {
                if (solidMenu)
                    jQuery("#menu").removeClass("hide-menu");
            }
            lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
        }, false);

    });
</script>

<script>
    function collapseMenu() {
        document.getElementById("menu-mobile-collapse").style.color = "red";

    }
    $(".showCollapseMenu").click(function(){
        $("#menu-mobile-collapse").addClass("animated fadeIn");
        $("#menu-mobile-collapse").removeClass("menu-hidden");

        $("#menu-mobile-items").addClass("animated bounceIn");
    }); 
    $(".hideCollapseMenu").click(function(){
        $("#menu-mobile-collapse").addClass("menu-hidden");

        $("#menu-mobile-items").removeClass("bounceIn");
    }); 
</script>

@endpush
