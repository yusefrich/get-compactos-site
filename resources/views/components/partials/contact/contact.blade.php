<div class="container-small mr-auto ml-auto top-spacing-contact">
    <div style="margin-bottom: 28px" class="  d-flex justify-content-center">
        <div class=" text-center">
            <h3 data-aos="fade-right"  class=" text-preto-azulado-get d-none d-md-block">
                Ficou com dúvidas? Let’s Get a coffee!
            </h3>
            <h1 data-aos="fade-right"  class=" text-preto-azulado-get d-md-none">
                Ficou com dúvidas? Let’s Get a coffee!
            </h1>
            <p data-aos="fade-right" data-aos-delay="300"  style=" max-width: 677px;" class=" text-preto-azulado-get">
                Preencha os campos abaixo com os seus dados e envie sua mensagem. Em breve entraremos em contato com você!
            </p>
        </div>
    </div>

    <div data-aos="fade-right" data-aos-delay="600" style="max-width: 555px;" class="mr-auto ml-auto px-4 px-md-0">
        {!! Form::open(['route' => 'contact.send', 'class' => 'text-start get-form']) !!}
        <div class="form-group ">
            <label for="input-name text-preto-azulado-get">Nome:</label>
            {{-- <input type="text" class="form-control " id="input-name" name="name" placeholder=""> --}}
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Seu Nome',
            'id'=> 'input-name', 'required']) !!}
        </div>
        <div class="form-group  ">
            <label for="input-email">Email:</label>
            {{-- <input type="email" class="form-control" id="input-email"  name="email" placeholder=""> --}}
            {!! Form::text('mail', null, ['class' => 'form-control','placeholder' =>
            'seu@email.com', 'id'=> 'input-email', 'required']) !!}
        </div>
        <div class="form-group    ">
            <label for="input-phone">Telefone:</label>
            {!! Form::text('phone', null, ['class' => 'form-control ', 'data-mask' => '(00) 00000-0000','placeholder' =>
            '(00) 00000-0000', 'id'=>
            'input-phone', 'required']) !!}
        </div>
        <div class="form-group    mx-0">
            <label for="input-mensagem">Mensagem:</label>
            {!! Form::textarea('msg', null, ['class' => 'form-control ','id'=>
            'input-mensagem', 'required']) !!}
        </div>
        <div class="d-flex justify-content-center pb-72">

            <button class="btn btn-outline-dark btn-block" type="submit">Enviar Contato</strong></button>
        </div>
        </form>
        
        
    </div>
    <div data-aos="fade-right"  class="text-center mb-20">
        <h3 class="text-preto-azulado-get d-none d-md-block">Fale com a gente</h3>
        <h1 class="text-preto-azulado-get d-md-none">Fale com a gente</h1>
        
    </div>
</div>
<div class="container-fluid mr-auto ml-auto position-relative">
    <img style="top: -300px;" class="hex-bg-pattern-contact d-none d-md-block contact-pattern" src="{{url('assets_front/imgs/hex-bg-pattern-full.png')}}" alt="">

    <div class="d-none d-md-block">
        <div data-aos="fade-right" data-aos-delay="300" style="padding-bottom: 139px" class="d-flex justify-content-center">
            <div class="d-flex">
                <p class="paragraph-20 text-vermelho-get  mb-0 mt-2 pt-1 pr-2">Instagram</p>
                <a class="btn btn-dark active btn-round p-2 my-2 mr-40" target="_blank" href="https://www.instagram.com/getcompactos/"><ion-icon style="font-size: 24px;" name="logo-instagram"></ion-icon></a>
            </div>
            <div class="d-flex">
                <p class="paragraph-20 text-vermelho-get  mb-0 mt-2 pt-1 pr-2">WhatsApp</p>
                <a class="btn btn-dark active btn-round p-2 my-2 mr-40" target="_blank" href="https://wa.me/5583996361415?text=Olá%20tenho%20interesse%20nos%20empreendimentos%20da%20get%20compactos."><ion-icon style="font-size: 24px;" name="logo-whatsapp"></ion-icon></a>
            </div>
            <div class="d-flex">
                <p class="paragraph-20 text-vermelho-get  mb-0 mt-2 pr-2 pt-1">Telefone</p>
                <h4 class="text-preto-azulado-get font-weight-bold mt-2 mb-0 pt-1"> 083 3031-9191</h4>
            </div>
        </div>
    </div>

    <div class="d-md-none mb-72">
        <div class="d-flex justify-content-center">
            <p class="paragraph-20 text-vermelho-get  mb-0 mt-2 pr-2 pt-1">Telefone</p>
            <h4 class="text-preto-azulado-get font-weight-bold mt-2 mb-0"> 083 3031-9191</h4>
        </div>
        <div class="d-flex justify-content-center">
            <a class="btn btn-dark active btn-round p-1 m-8" target="_blank" href="https://www.instagram.com/getcompactos/"><ion-icon style="font-size: 24px;" name="logo-instagram"></ion-icon></a>
            <a class="btn btn-dark active btn-round p-1 m-8" target="_blank" href="https://wa.me/5583996361415?text=Olá%20tenho%20interesse%20nos%20empreendimentos%20da%20get%20compactos."><ion-icon style="font-size: 24px;" name="logo-whatsapp"></ion-icon></a>
        </div>

    </div>

</div>
