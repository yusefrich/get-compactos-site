<div class="background-cinza">

<div class="container-small mr-auto ml-auto pt-72  pb-120 pb-mob-72">{{-- pt-mob-200 --}}
    <div style="margin-bottom: 28px" class="  d-flex justify-content-center">
        <div class=" text-center">       
            <h2 data-aos="fade-right" class=" text-preto-azulado-get">Ficou interessado?</h2>
            <p data-aos="fade-right" data-aos-delay="200" style=" max-width: 544px;" class=" text-preto-azulado-get">
                Preencha os campos abaixo com os seus dados e envie sua mensagem. Em breve entraremos em contato com você!
            </p>
        </div>
    </div>

    <div data-aos="fade-right" data-aos-delay="400" style="max-width: 555px;" class="mr-auto ml-auto px-3 px-md-0">
        {!! Form::open(['route' => ['emp.contact', $data->slug], 'class' => 'text-start get-form']) !!}
        <div class="form-group ">
            <label for="input-name text-preto-azulado-get">Nome:</label>
            {{-- <input type="text" class="form-control " id="input-name" name="name" placeholder=""> --}}
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Seu Nome',
            'id'=> 'input-name', 'required']) !!}
        </div>
        <div class="form-group  ">
            <label for="input-email">Email:</label>
            {{-- <input type="email" class="form-control" id="input-email"  name="email" placeholder=""> --}}
            {!! Form::text('mail', null, ['class' => 'form-control','placeholder' =>
            'seu@email.com', 'id'=> 'input-email', 'required']) !!}
        </div>
        <div class="form-group    ">
            <label for="input-phone">Telefone:</label>
            {!! Form::text('phone', null, ['class' => 'form-control ', 'data-mask' => '(00) 00000-0000','placeholder' => '(00) 00000-0000', 'id'=>
            'input-phone', 'required']) !!}
        </div>
        <div class="form-group    mx-0">
            <label for="input-mensagem">Mensagem:</label>
            {!! Form::textarea('msg', null, ['class' => 'form-control ','id'=>
            'input-mensagem', 'required']) !!}
        </div>
        <div class="d-flex justify-content-center">

            <button class="btn btn-outline-dark btn-block   " type="submit">Enviar </strong></button>
        </div>
        </form>


    </div>

</div>
</div>
