<div class="container-fluid">
    <div class="row mt-72 mb-20">
        <div class="col-md-6 d-flex justify-content-end">
            <div class="project">
                <span data-aos="fade-right" class="text-uppercase m-0 text-vermelho-get">O Projeto</span>
                @if(!empty($data->title))
                <h2 data-aos="fade-right" class="text-preto-azulado-get">
                    {{ $data->title}}
                </h2>
                @endif
                @if(!empty($data->txt))
                <p data-aos="fade-right" class="text-dark">
                    {!! str_replace('\n', '<br>', nl2br($data->txt)) !!}
                </p>
                @endif
            </div>
        </div>
        
        <div style="padding-left: 69px" class="col-md-6">
            @if(!empty($data->img1))
            <img data-aos="flip-right" data-aos-delay="300" class="img-cover emp1-img"
                src="{{url('storage/emp/'.$data->img1)}}" alt="">
            @endif
        </div>
    </div>
    {{--fixa tecnico ************************* --}}
    @if(!empty($data->column1) || !empty($data->column2))
    <div class="justify-content-center my-50 d-none d-md-flex">
        <div data-aos="fade-right" class="position-relative mr-40" style="max-width: 304px; width: 100%;">
            <h2 class="text-preto-azulado-get v-center position-inherit ">Ficha Técnica</h2>
        </div>
        @if(!empty($data->column1))
        <div class="ml-40 mr-20" style="max-width: 264px; width: 100%;">
            <p data-aos="fade-right" data-aos-delay="300" class="font-weight-bold lh-42">
                {!! str_replace('\n', '<br>', nl2br($data->column1)) !!}
            </p>
        </div>
        @endif
        @if(!empty($data->column2))
        <div class="mx-9" style="max-width: 261px;">
            <p data-aos="fade-right" data-aos-delay="600" class="font-weight-bold lh-42">
                {!! str_replace('\n', '<br>', nl2br($data->column2)) !!}
            </p>
        </div>
        @endif
    </div>
    @endif
    <div class="d-md-none">
        <div data-aos="fade-right" class="position-relative " style="max-width: 304px; width: 100%;">
            <h2 class="text-preto-azulado-get  position-inherit mt-4 mb-3 pt-1">Ficha Técnica</h2>
        </div>
        <div class="" style="max-width: 264px; width: 100%;">
            <p data-aos="fade-right" data-aos-delay="300" class="font-weight-bold lh-42 mb-0">
                {!! str_replace('\n', '<br>', nl2br($data->column1)) !!}
            </p>
        </div>
        <div style="max-width: 261px;">
            <p data-aos="fade-right" data-aos-delay="600" class="font-weight-bold lh-42">
                {!! str_replace('\n', '<br>', nl2br($data->column2)) !!}
            </p>
        </div>

    </div>
    @php $showCaracteristicas = false @endphp
    @for($i=1;$i<=15;$i++) 
        @php $icon='icon' .$i; $txt='txt' .$i; @endphp
        @if(($data->$txt !== null) && ($data->$txt !== ""))
            @php $showCaracteristicas = true @endphp
        @endif
    @endfor

    {{-- caracteristicas *********************** --}}
    @if($showCaracteristicas)
    <div class="d-none d-md-flex justify-content-center my-50">
        <div data-aos="fade-up" class="position-relative ">
            <h2 class="text-preto-azulado-get v-center position-inherit mt-20" style="max-width: 347px;">Caracteristicas</h2>
        </div>
        <div class="ml-40 mr-20" style="max-width: 520px;">
            <div class="row empreendimentos-icons-spacing  ">
                @for($i=1;$i<=15;$i++) @php $icon='icon' .$i; $txt='txt' .$i; @endphp @if(($data->$txt !== null) && ($data->$txt !== ""))
                    <div data-aos="fade-up" data-aos-delay="100" class="col-4">
                        <i class="text-dark icon icon-caracteristicas {{$data->$icon}}"></i>
                        <p class="caption-16 text-dark m-0">{{$data->$txt}}</p>
                    </div>
                    @endif
                @endfor
            </div>
        </div>
    </div>
    <div class="d-md-none">
        <div data-aos="fade-up" class="position-relative">
            <h2 class="text-preto-azulado-get position-inherit  mt-4 mb-3 pt-1" >Caracteristicas</h2>
        </div>
        <div class="row  mx-0 ">
            @for($i=1;$i<=15;$i++) @php $icon='icon' .$i; $txt='txt' .$i; @endphp @if(($data->$txt !== null) && ($data->$txt !== ""))
                <div data-aos="fade-up" data-aos-delay="100" class="col-4 mb-20">
                    <i class="text-dark icon icon-caracteristicas {{$data->$icon}}"></i>
                    <p class="caption-16 text-dark m-0">{{$data->$txt}}</p>
                </div>
                @endif
            @endfor
        </div>

    </div>
    @endif

</div>