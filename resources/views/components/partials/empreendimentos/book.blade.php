@if(!empty($data->book))
<div data-aos="fade-up"  class="container-fluid bg-dark p-0">
    <div class="row pt-72 pb-120 pb-mob-42 pt-mob-32 m-0 overflow-hidden">
        <div class="col-md-6 d-flex justify-content-center justify-content-md-end ">
            <div class="book text-center text-md-left">
                <h2 data-aos="fade-right" data-aos-delay="200"  class="text-white d-none d-md-block" style="max-width: 470px;">Baixe o book e conheça este imóvel</h2>
                <h2 data-aos="fade-right" data-aos-delay="200"  class="text-white d-md-none text-center mx-auto" style="max-width: 384px;">Baixe o book e conheça este imóvel</h2>
                <p data-aos="fade-right" data-aos-delay="400"  class="text-white d-none d-md-block" style="max-width: 460px;">
                    Inovação e autenticidade, muito mais do que um compacto. Baixe o nosso book e conheça o empreendimento. 
                </p>
                <p data-aos="fade-right" data-aos-delay="400"  class="text-white span-16 text-center d-md-none mb-40 " style="max-width: 460px;">
                    Inovação e autenticidade, muito mais do que um compacto. Baixe o nosso book e conheça o empreendimento. 
                </p>
                <a data-aos="fade-right" data-aos-delay="600"  class="btn btn-outline-primary d-none d-md-block" href="{{route('emp.download', $data->slug)}}" target="_BLANK">Baixar Agora</a>
            </div>
        </div>
        <div style="padding-left: 84px" class="col-md-6 d-none d-md-block">
            <img 
            style="top: -31px;
                left: 46px;
                max-width: 531px;"class="img-bg-pattern-left" src="{{url('assets_front/imgs/img-bg-pattern-white.svg')}}" alt="">
            <img data-aos="flip-right" data-aos-delay="400"   class="img-cover book-img" src="{{url('storage/emp/'.$data->imgbook)}}" alt="">
        </div>
        <div class="col-12 d-md-none mb-20 pb-2 text-center">
            <img 
            style="top: -31px;
                left: 0px;
                right: 0px;
                max-width: 531px;"class="img-bg-pattern-left mx-auto" src="{{url('assets_front/imgs/img-bg-pattern-white.svg')}}" alt="">
            <img data-aos="flip-right" data-aos-delay="400" style="height: 213px;"  class="img-cover book-img" src="{{url('storage/emp/'.$data->imgbook)}}" alt="">
        </div>
        <div class="col-12 text-center d-md-none">
            <a data-aos="fade-right" data-aos-delay="600"  class="btn btn-outline-primary " href="{{route('emp.download', $data->slug)}}" target="_BLANK">Baixar Agora</a>
        </div>

    </div>

</div>
@endif

