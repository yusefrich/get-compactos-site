@if(!empty($data->lat) && !empty($data->long))
<div  class="container-fluid p-0 overflow-hidden position-relative bg-dark">
    <div class="mapFade d-none d-md-block"></div>
    <div class="mapFadeMob d-md-none"></div>
    <div style="opacity: 1;     width: 110%;" id="map" class="mapLocations d-none d-md-block"></div>{{-- .6 --}}
    <div style="opacity: 1;" id="mapMobile" class="mapLocations d-md-none"></div>{{-- .6 --}}
    <div class="container py-116 pt-mob-42 pb-mob-300">
        <h3 data-aos="fade-up" data-aos-delay="200" style="max-width: 324px;" class="d-none d-md-block text-white mb-20 text-over-map">Uma localização privilegiada </h3>
        <h1 data-aos="fade-up" data-aos-delay="200" style="max-width: 324px;" class="d-md-none text-white mb-20 text-center mx-auto text-over-map">Uma localização privilegiada </h1>

        <div data-aos="fade-up" data-aos-delay="400" class="d-flex justify-content-md-start justify-content-center text-white">

            <div class="d-none d-md-block" style="max-width: 150px; margin-right: 45px">
            @php
                $keyValue = 0;
            @endphp
            @foreach($pins as $key => $pin)
                @if ($key%3==0 && $key > 0)
                    <div class="d-none d-md-block" style="max-width: 150px; margin-right: 45px">
                @endif
                    <button class="btn mapFilter p-0 text-over-map" data-name={{json_encode($pin['slug'] ?? '')}} data-icon="{{url('storage/pin/'.$pin['color'])}}">
                        <p class="text-white "> <img src="{{url('storage/pin/'.$pin['img'])}}" alt=""> {{ $pin['cat'] }}</p>
                    </button><br>
                @if ($key%3==2)
                    </div>
                @endif
                @php
                    $keyValue = $key%3;
                @endphp
            @endforeach
            
            @if ($keyValue!=2)
                </div>
            @endif

            {{-- <div class="d-none d-md-block" style="max-width: 133px; margin-right: 45px">
                <button class="btn mapFilter p-0 text-over-map" data-name="bar">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-bar.png')}}" alt=""> Bares</p>
                </button>
                <button class="btn mapFilter p-0 text-over-map" data-name="restaurant">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-restaurant.png')}}" alt=""> Restaurants</p>
                </button>
                <button class="btn mapFilter p-0 text-over-map" data-name="supermarket">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-shopping.png')}}" alt=""> Mercado</p>
                </button>
            </div> --}}
            <div class="d-md-none mx-3" >
                @php
                    $keyValue = 0;
                @endphp
                @foreach($pins as $key => $pin)
                    @if ($key%3==0 && $key > 0)
                        <div class="d-md-none mx-3" >
                    @endif
                        <button class="btn mapFilter p-0 text-over-map" data-name={{json_encode($pin['slug'] ?? '')}} data-icon="{{url('storage/pin/'.$pin['color'])}}">
                            <p class="text-white "> <img src="{{url('storage/pin/'.$pin['img'])}}" alt=""> {{ $pin['cat'] }}</p>
                        </button><br>
                    @if ($key%3==2)
                        </div>
                    @endif
                    @php
                        $keyValue = $key%3;
                    @endphp
                @endforeach
                
                @if ($keyValue!=2)
                    </div>
                @endif
    
            {{-- <div class="d-md-none ml-14">
                <button class="btn mapFilter p-0 text-over-map" data-name="bar">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-bar.png')}}" alt=""> Bares</p>
                </button>
                <button class="btn mapFilter p-0 text-over-map" data-name="restaurant">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-restaurant.png')}}" alt=""> Restaurants</p>
                </button>
                <button class="btn mapFilter p-0 text-over-map" data-name="supermarket">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-shopping.png')}}" alt=""> Mercado</p>
                </button>
            </div> --}}

            {{-- <div class="d-none d-md-block" style="max-width: 133px; margin-right: 45px">
                <button class="btn mapFilter p-0 text-over-map" data-name="hospital">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-clinics.png')}}" alt=""> Clínicas</p>
                </button>
                <button class="btn mapFilter p-0 text-over-map" data-name="school">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-education.png')}}" alt=""> Escolas</p>
                </button>
                <button class="btn mapFilter p-0 text-over-map" data-name="tourist_attraction">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-beach.png')}}" alt=""> Praia</p>
                </button>
            </div> --}}
            {{-- <div class="d-md-none ml-14">
                <button class="btn mapFilter p-0 text-over-map" data-name="hospital">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-clinics.png')}}" alt=""> Clínicas</p>
                </button>
                <button class="btn mapFilter p-0 text-over-map" data-name="school">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-education.png')}}" alt=""> Escolas</p>
                </button>
                <button class="btn mapFilter p-0 text-over-map" data-name="tourist_attraction">
                    <p class="text-white"> <img src="{{url('assets_front/imgs/icon-beach.png')}}" alt=""> Praia</p>
                </button>
            </div> --}}
        </div>
    </div>
</div>

@push('scripts')
    
<script>
    var currentMarkers = [];


    function initMap() {
        var thedata = {!! json_encode($data) !!}
        var pins = {!! json_encode($pins) !!}
        console.log("all the pins data");
        console.log(pins);

        var infowindow;
        infowindow = new google.maps.InfoWindow();

        var myPlace = {lat: +thedata.lat, lng: +thedata.long};

        var mapStyles = [
            {elementType: 'geometry', stylers: [{color: '#212121'}]}, /* 242f3e */
            {elementType: 'labels.text.stroke', stylers: [{color: '#373737'}]}, /* 333333 */
            {elementType: 'labels.text.fill', stylers: [{color: '#333333'}]},
            {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{color: '#eeeeee'}]
            },
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#ffffff'}, { visibility: "off" }]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{color: '#263c3f'}]
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{color: '#ffffff'}, { lightness: 40 }]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#282828'}]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{color: '#333333'}]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{color: '#5b5e62'}]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#5b5e62'}, { visibility: "off" }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#3f3930'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#1f2835'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{color: '#373737'}]
            },
            {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{color: '#2f3948'}]
            },
            {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{color: '#eeeeee'}]
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{color: '#2B2B2B'}]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#515c6d'}]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#2B2B2B'}]
            }
        ];
      // Styles a map in night mode.
        var mapEl = document.getElementById('map');
        var mapElMob = document.getElementById('mapMobile');
        var map;
        var mapMob;
        if(mapEl){
            map = new google.maps.Map(mapEl, {
                center: myPlace,
                zoom: 16,
                disableDefaultUI: true,
                styles: mapStyles
            });
        }
        if(mapElMob){
            mapMob = new google.maps.Map(mapElMob, {
                center: myPlace,
                zoom: 16,
                disableDefaultUI: true,
                styles: mapStyles
            });
        }
        var mainContentString = '<div id="content">'+
                                    '<div id="siteNotice">'+
                                    '</div>'+
                                    '<h5 id="firstHeading" class="firstHeading text-dark mb-0"> {!! $data->ch_title !!} </h5>'+
                                    '<div id="bodyContent">'+
                                    '</div>'+
                                    '</div>';

        var mainInfowindow = new google.maps.InfoWindow({
            content: mainContentString
        });

        var markerMain = new google.maps.Marker(
            {
                position: myPlace, 
                title: "Empreendimento", 
                map: map,
                icon: '{!! url('assets_front/imgs/marker.png') !!}'
            }
        );
        var markerMainMob = new google.maps.Marker(
            {
                position: myPlace, 
                title: "Empreendimento", 
                map: mapMob,
                icon: '{!! url('assets_front/imgs/marker.png') !!}'
            }
        );

        markerMain.addListener('click', function() {
            mainInfowindow.open(map, markerMain);
        });
        markerMainMob.addListener('click', function() {
            mainInfowindow.open(mapMob, markerMainMob);
        });

        var filterIcon = "";

        function callback(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                for (var i = 0; i < results.length; i++) {
                    createMarker(results[i]);
                }
                console.log("status okkk");
            }
        }
        function createMarker(place) {
            var placeLoc = place.geometry.location;
            var marker = new google.maps.Marker({
                map : map,
                position : place.geometry.location,
                icon: filterIcon
            });
            var mobmarker = new google.maps.Marker({
                map : mapMob,
                position : place.geometry.location,
                icon: filterIcon
            });

            currentMarkers.push(marker);
            currentMarkers.push(mobmarker);

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(place.name);
                infowindow.open(map, this);
            });
            google.maps.event.addListener(mobmarker, 'click', function() {
                infowindow.setContent(place.name);
                infowindow.open(mapMob, this);
            });
        }

        var service = new google.maps.places.PlacesService(map);


        $( ".mapFilter" ).bind( "click", function(event) {
            currentMarkers.forEach(cm => {
                cm.setMap(null);
            });

            currentMarkers = [];

            var filterValue = $(this).data("name").substring(1, $(this).data("name").length-1);

            console.log("filtro sendo passado");
            console.log(''+filterValue);
            filterIcon = $(this).data("icon");


            service.nearbySearch({
                location : myPlace,
                radius : 5500,
                type : [ ''+filterValue ]
            }, callback);
        });




        /* $( ".mapFilter" ).bind( "click", function(event) {
            currentMarkers.forEach(cm => {
                cm.setMap(null);
            });

            currentMarkers = [];

            var pins = $(this).data("filter");

            pins.addresses.forEach(e => {
                var contentString = '<div id="content">'+
                                    '<div id="siteNotice">'+
                                    '</div>'+
                                    '<h5 id="firstHeading" class="firstHeading text-dark mb-0">' + e.name + '</h5>'+
                                    '<div id="bodyContent">'+
                                    '</div>'+
                                    '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                var marker = new google.maps.Marker({
                    position: {lat: +e.lat, lng: +e.long},
                    map: map,
                    animation: google.maps.Animation.DROP,
                    title: e.name,
                    // icons: '{!! url('storage/pin/pins.img') !!}' 
                });
                marker.addListener('click', toggleBounce);
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });

                marker.setMap(map);
                currentMarkers.push(marker);
            });

        }); */

    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?key={{config('app.map_key')}}&libraries=places&callback=initMap"
async defer></script>

@endpush
@endif
