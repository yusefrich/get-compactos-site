@php
    $url = $data->background ? 'storage/emp/'.$data->background : 'assets_front/imgs/empreendimento-bg.jpg';
@endphp
<div style="background:linear-gradient(0deg, rgba(40, 40, 40, 0.6), rgba(40, 40, 40, 0.6)), url('{{url($url)}}');
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
            position: relative;
            width: 100%;" class="banner-smaller-height">
    <div class="bg-banner-black d-none d-md-block"></div>

    <div class=" v-center v-mob-normal container-xl mr-auto ml-auto">
        <div data-aos="fade-up" class="social-banner text-center d-none d-md-block">
            <p class="mt-5">083 3031-9191</p>
            <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="tel:83-3031-9191">
                <ion-icon style="font-size: 24px;" name="call"></ion-icon>
            </a>
            <div class="vl"></div>
            <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="https://wa.me/5583996361415?text=Olá%20tenho%20interesse%20nos%20empreendimentos%20da%20get%20compactos.">
                <ion-icon style="font-size: 24px;" name="logo-whatsapp"></ion-icon>
            </a>
            <div class="vl"></div>
            <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="https://www.instagram.com/getcompactos/">
                <ion-icon style="font-size: 24px;" name="logo-instagram"></ion-icon>
            </a>
        </div>
        <div class="d-flex justify-content-center">

            <div class="mt-md-170 text-center text-md-left" style="max-width: 541px;">
                <img data-aos="fade-right" data-aos-delay="300" class="icon-emp " src="{{url('storage/emp/'.$data->logo)}}" alt="">
                <h1 data-aos="fade-right" data-aos-delay="600" style="max-width: 541px;">{{$data->head_title}}</h1>
                <p data-aos="fade-right" data-aos-delay="900" style="max-width: 504px;" class="text-white">{!! $data->head_subtitle !!}</p>
            </div>
        </div>
        <div class=" d-md-none">

            <div class="d-flex justify-content-center pb-20">
                <div class="mt-40 mb-50">
                    <p  class="text-white mb-0 ml-2">|</p>
                    <ion-icon style="font-size: 22px" class="text-white" name="chevron-down-outline"></ion-icon>
                </div>
            </div>
        </div>

    </div>
</div>

@if(count($data->floors) > 0)
<a href="../monteoseu?e={{$data->id}}" class="click-link link-monteoseu">
    <div class="link-monteoseu-holder">
        <p class="caption-26">Monte o seu</p>
        <img src="{{url('assets_front/imgs/btn-monteoseu.png')}}" alt="">
    </div>
</a>
@endif