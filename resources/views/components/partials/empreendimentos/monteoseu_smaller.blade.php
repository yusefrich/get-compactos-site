{{-- monte-smaller-bg.jpg --}}
@if(count($data->floors) > 0)
    <div data-aos="fade-up" data-aos-delay="100"  style="
            background: url('{{url('assets_front/imgs/monte-smaller-bg-2.png')}}');" {{-- linear-gradient(0deg, rgba(38, 38, 38, 0.5), rgba(38, 38, 38, 0.5)),  --}}
        class="container-fluid banner-smaller text-center my-42">
        <img data-aos="fade-right" data-aos-delay="200"  class="icon-emp" src="{{url('assets_front/imgs/building-icon.svg')}}" alt="">
        {{-- style="background-image: url('{{url('/storage/home/'.$img->img)}}')" --}}
        <h3 data-aos="fade-right" class="text-white d-none d-md-block" data-aos-delay="300" >
            Monte seu compacto
        </h3>
        <h1 data-aos="fade-right" class="text-white d-md-none" data-aos-delay="300" >
            Monte seu compacto
        </h1>
        <p data-aos="fade-right" data-aos-delay="400" class="text-white mr-auto ml-auto mb-20 d-none d-md-block" style="max-width: 496px;">
            Monte a unidade que combina com seu estilo de vida, com a sua personalidade e que cabe no seu bolso.
        </p>
        <p data-aos="fade-right" data-aos-delay="400" class="span-16 text-white mr-auto ml-auto mb-20 d-md-none" style="max-width: 496px;">
            Monte a unidade que combina com seu estilo de vida, com a sua personalidade e que cabe no seu bolso.
        </p>
        <a href="../monteoseu?e={{$data->id}}" data-aos="fade-right" data-aos-delay="500"  class="btn btn-outline-primary">Montar Agora</a>
    </div>
@endif
