<li class="nav-item @if(Route::is('about')) active @endif">
    <a class="nav-link caption-18 fw-600" href="{{ route('about') }}">Sobre a Get</a>
</li>
<li class="nav-item @if(Route::is('builder')) active @endif">
    <a class="nav-link caption-18 fw-600" href="{{ route('builder') }}">Monte o Seu</a>
</li>
<li class="nav-item @if(Route::is('empreendimentos') || request()->is('empreendimentos/*')) active @endif">
    <a class="nav-link caption-18 fw-600" href="{{ route('empreendimentos') }}">Empreendimentos</a>
</li>
<li class="nav-item @if(Route::is('conteudos') || request()->is('conteudos/*')) active @endif">
    <a class="nav-link caption-18 fw-600" href="{{ route('conteudos') }}">Conteúdos</a>
</li>
<li class="nav-item @if(Route::is('contact')) active @endif">
    <a class="nav-link caption-18 fw-600" href="{{ route('contact') }}">Contato</a>
</li>
