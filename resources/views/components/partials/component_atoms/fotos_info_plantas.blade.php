<div class="carousel-item @if($key == 0) active @endif">
    @php $val = 1; @endphp
    @if($photo['1stt'] != null || $photo['1tt'] != null)
    <div class="img-info s1" >
        <div class="point-info mx-auto" >
            <span>{{$val}}</span>
            @php $val++ @endphp
        </div>
        <div class="info-hover" >
            <h4 class="text-center text-preto-azulado-get">{{$photo['1tt']}}</h4>
        <p class="paragraph-20 text-preto-azulado-get text-center">{{$photo['1stt']}}</p>
        </div>
    </div>
    @endif
    @if($photo['2stt'] != null || $photo['2tt'] != null)
    <div class="img-info s2" >
        <div class="point-info mx-auto" >
            <span>{{$val}}</span>
            @php $val++ @endphp
        </div>
        <div class="info-hover" >
            <h4 class="text-center text-preto-azulado-get">{{$photo['2tt']}}</h4>
        <p class="paragraph-20 text-preto-azulado-get text-center">{{$photo['2stt']}}</p>
        </div>
    </div>
    @endif
    @if($photo['3stt'] != null || $photo['3tt'] != null)
    <div class="img-info s3" >
        <div class="point-info mx-auto" >
            <span>{{$val}}</span>
            @php $val++ @endphp
        </div>
        <div class="info-hover" >
            <h4 class="text-center text-preto-azulado-get">{{$photo['3tt']}}</h4>
        <p class="paragraph-20 text-preto-azulado-get text-center">{{$photo['3stt']}}</p>
        </div>
    </div>
    @endif
    @if($photo['4stt'] != null || $photo['4tt'] != null)
    <div class="img-info s4" >
        <div class="point-info mx-auto" >
            <span>{{$val}}</span>
            @php $val++ @endphp
        </div>
        <div class="info-hover" >
            <h4 class="text-center text-preto-azulado-get">{{$photo['4tt']}}</h4>
        <p class="paragraph-20 text-preto-azulado-get text-center">{{$photo['4stt']}}</p>
        </div>
    </div>
    @endif
    @if($photo['5stt'] != null || $photo['5tt'] != null)
    <div class="img-info s5" >
        <div class="point-info mx-auto" >
            <span>{{$val}}</span>
            @php $val++ @endphp
        </div>
        <div class="info-hover" >
            <h4 class="text-center text-preto-azulado-get">{{$photo['5tt']}}</h4>
        <p class="paragraph-20 text-preto-azulado-get text-center">{{$photo['5stt']}}</p>
        </div>
    </div>
    @endif
    @if($photo['6stt'] != null || $photo['6tt'] != null)
    <div class="img-info s6" >
        <div class="point-info mx-auto" >
            <span>{{$val}}</span>
            @php $val++ @endphp
        </div>
        <div class="info-hover" >
            <h4 class="text-center text-preto-azulado-get">{{$photo['6tt']}}</h4>
        <p class="paragraph-20 text-preto-azulado-get text-center">{{$photo['6stt']}}</p>
        </div>
    </div>
    @endif

    <img class="d-block w-100 img-cover img-fotos-planta" src="{{url('storage/emp/'.$photo->img)}}" alt="First slide">
</div>
