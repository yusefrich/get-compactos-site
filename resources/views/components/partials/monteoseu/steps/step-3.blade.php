<div class="row">{{-- unities --}}
    <div class="col-md-5 text-left mx-2 mx-md-0">
        <div class="ml-0 ml-md-4 mt-32">
            <p class="animated fadeInLeft  caption-14 text-hex-797979">
                Passo 3 de 8
            </p>
            <h4 class=" animated fadeInLeft delay-200ms text-preto-azulado-get">
                Escolha a planta
            </h4>
            <p style="max-width: 360px;" class="animated fadeInLeft delay-400ms caption-15 text-preto-azulado-get mb-0 mb-md-5 pb-4">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget felis ultrices enim nisi, sed. Vitae eu sagittis.
            </p>
            <div onclick="openSelectStepDataMobile('#step-3-mobile-data')" class="animated fadeInLeft delay-400ms select-step-mobile d-flex d-md-none mb-4 justify-content-between">
                <div class="d-flex justify-content-start">
                    <p id="step-3-mobile-data-label" class="caption-15 font-weight-bold text-hex-797979 mb-0">Selecione aqui a planta</p>
                    <span id="step-3-mobile-data-value" class="caption-15 font-weight-normal text-hex-797979 mb-0 float-left ml-2"></span>
                </div>
                <ion-icon name="chevron-down-outline"></ion-icon>
            </div>

            <div id="unities-select-area" class="animated fadeInLeft delay-600ms d-none d-md-block">
            </div>


        </div>
    </div>
    <div data-aos="fade" class="col-md-7  item-detail-col">
        <div onclick="openModalItemDetail('#modal_unity_detail')" id="current-unity-select-area"
            class="item-detail-img h-mob-64 mr-0 mr-md-4"
            style="background-image:  url('{{url('assets_front/imgs/detail-bg-planta.png')}}');">
            <p class="caption-14 text-middle-gray item-detail-detail mb-0 value-partial-title">Valor parcial</p>
            <h4  class="ml-32 text-middle-gray  value-partial-class font-weight-bold"></h4>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt="">
        </div>
    </div>
</div>

@push('modals')
<div class="modal fade p-0" id="modal_unity_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal fm-monte-o-seu" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button style="color: #fff; z-index: 90" type="button" class="close btn   p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon size="large" name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="zoom transition">
                    <img class="modal-item-img" id="modal_unity_detail-img" src="" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade pr-0" id="step-3-mobile-data" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog fullscreen-modal-white">
      <div class="modal-content">
        <div style="border-bottom: 0" class="modal-header">
          <button type="button" class="close mr-auto ml-0" data-dismiss="modal" aria-label="Close">
            <ion-icon size="large" name="chevron-down-outline"></ion-icon>
          </button>
        </div>
        <div class="modal-body">
            <div id="unities-select-area-mob" class="">
            </div>
        </div>
      </div>
    </div>
  </div>

@endpush
@push('scripts')
<script>
    function selectUnity(unities, floor, unityName, obj) {
        setParcialValue("3", $(obj).data('minimum'));
        SetSelectedStep("3", obj);

        var mobileValueFormatted = $(obj).data('minimum');
        $("#step-3-mobile-data-value").html(mobileValueFormatted.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}));

        var imgToSet = "";
        unities.forEach(e => {
            if(e.img2){imgToSet = e.img2; return;}
        });
        setItemMOSBlack("#current-unity-select-area", "#modal_unity_detail-img", `{{url('storage/unity/${imgToSet}')}}`);

        $('#disabled-planta-selec').removeAttr("hidden");

        var uniHtml = '';
        console.log("selectUnity being called");
        console.log(unities);
        uniHtml = uniHtml+ '<div class="row">';
        unities.forEach(element => {
            uniHtml = uniHtml+
                '<div data-aos="fade" class="col-md-4 builder-card  pb-3">'+
                    `<div class="d-flex">`;
            if(element.sold == 0){
                uniHtml = uniHtml+ 
                        `<div  class="custom-control check-emp-offset custom-radio get-select">`+
                            `<input onclick='return selectAp(${JSON.stringify(element)}, ${JSON.stringify(floor)}, ${JSON.stringify(element)}, this)' style="box-shadow: none" type="radio"  name="plantaSelectRadio" data-value="${element.price}" class="custom-control-input get-check-emp select-unidade" aria-describedby="movel-value-" id="planta-check-${element.id}">`+
                            `<label class="no-wrap-text click-link custom-control-label text-preto-azulado-get" for="planta-check-${element.id}">`+
                                `<p class=" text-preto-azulado-get caption-16 mb-0">${element.name}</p>`+
                                `<p class="click-link caption-14 text-hex-797979 mb-0">+${element.price}</p>`+
                            `</label>`+
                        `</div>`;
            }else{
                uniHtml = uniHtml+ 
                        `<div  class="custom-control check-emp-offset custom-radio get-select">`+
                            `<input onclick='return selectAp(${JSON.stringify(element)}, ${JSON.stringify(floor)}, ${JSON.stringify(element)}, this)' style="box-shadow: none" type="radio"  name="plantaSelectRadio"  class="custom-control-input get-check-emp" aria-describedby="movel-value-" id="planta-check-${element.id}">`+
                            `<label class="no-wrap-text click-link custom-control-label text-preto-azulado-get" for="planta-check-${element.id}">`+
                                `<p class=" text-preto-azulado-get caption-16 mb-0">${element.name}</p>`+
                                `<p style="color: #FF3000;" class="click-link caption-14 mb-0">VENDIDO</p>`+
                            `</label>`+
                        `</div>`;
            }

            uniHtml = uniHtml+ 
                    `</div>`+
                `</div>`;
        });
        uniHtml = uniHtml+'</div>';

        /* $('#disabled-uni-selec').removeAttr("disabled"); */
        $('#ap-select-area').html(uniHtml);

        uniHtml = '';
        uniHtml = uniHtml+ '<div class="row">';
        unities.forEach(element => {
            uniHtml = uniHtml+
                '<div data-aos="fade" class="col-md-4 builder-card  pb-3">'+
                    `<div class="d-flex">`;
            if(element.sold == 0){
                uniHtml = uniHtml+ 
                        `<div  class="custom-control check-emp-offset custom-radio get-select ">`+
                            `<input onclick='return selectAp(${JSON.stringify(element)}, ${JSON.stringify(floor)}, ${JSON.stringify(element)})' style="box-shadow: none" type="radio"  name="plantaSelectRadio" data-value="${element.price}" class="custom-control-input get-check-emp select-unidade get-free-select" aria-describedby="movel-value-" id="planta-check-mob-${element.id}">`+
                            `<label class="no-wrap-text click-link custom-control-label text-preto-azulado-get" for="planta-check-mob-${element.id}">`+
                                `<p class=" text-preto-azulado-get caption-16 mb-0">${element.name}</p>`+
                                `<p class="click-link caption-14 text-hex-797979 mb-0">+${element.price}</p>`+
                            `</label>`+
                        `</div>`;
            }else{
                uniHtml = uniHtml+ 
                        `<div  class="custom-control check-emp-offset custom-radio get-select ">`+
                            `<input disabled onclick='return selectAp(${JSON.stringify(element)}, ${JSON.stringify(floor)}, ${JSON.stringify(element)})' style="box-shadow: none" type="radio"  name="plantaSelectRadio"  class="custom-control-input get-check-emp get-sold-input" aria-describedby="movel-value-" id="planta-check-mob-${element.id}">`+
                            `<label class="no-wrap-text click-link custom-control-label text-preto-azulado-get" for="planta-check-mob-${element.id}">`+
                                `<p class=" text-preto-azulado-get caption-16 mb-0">${element.name}</p>`+
                                `<p style="color: #FF3000;" class="click-link caption-14 mb-0">VENDIDO</p>`+
                            `</label>`+
                        `</div>`;
            }

            uniHtml = uniHtml+ 
                    `</div>`+
                `</div>`;
        });
        uniHtml = uniHtml+'</div>';

        $('#ap-select-area-mob').html(uniHtml);
        

        $("#step-3-mobile-data").modal('hide')

        $("#step-3-mobile-data-label").html(unityName);
        $("#step-3-mobile-data-label").removeClass("text-hex-797979");
        $("#step-3-mobile-data-label").addClass("text-middle-gray");

    }

</script>
@endpush