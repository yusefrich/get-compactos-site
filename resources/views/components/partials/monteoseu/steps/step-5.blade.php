{{-- <div class="text-center mx-4 mx-md-0">
    <img src="{{url('assets_front/imgs/forniture-icon.png')}}" class="pb-2" alt="">
    <h4 class="text-preto-azulado-get">
        Móveis planejados
    </h4>
    <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" id="planejados-desc" style="max-width: 380px;">
        Selecione o projeto de móveis planejados e siga para a próxima etapa 
    </p>
</div>
<div class="d-none d-md-flex justify-content-center mb-40 planned-holder ">
    
</div>
<div class=" mb-40 planned-holder-mobile d-md-none">
    
</div> --}}

<div class="row">
    <div class="col-md-5 text-left mx-2 mx-md-0">
        <div class="ml-0 ml-md-4 mt-32">
            <p class="animated fadeInLeft caption-14 text-hex-797979">
                Passo 5 de 8
            </p>
            <h4 class="animated fadeInLeft delay-200ms text-preto-azulado-get">
                Móveis planejados
            </h4>
            <p style="max-width: 360px;" class="animated fadeInLeft delay-400ms caption-15 text-preto-azulado-get mb-0 mb-md-5 pb-4">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget felis ultrices enim nisi, sed. Vitae eu sagittis.
            </p>
            <div onclick="openSelectStepDataMobile('#step-5-mobile-data')" class="animated fadeInLeft delay-400ms select-step-mobile d-flex d-md-none mb-4 justify-content-between">
                <div class="d-flex justify-content-start">
                    <p id="step-5-mobile-data-label" class="caption-15 font-weight-bold text-hex-797979 mb-0">Selecione aqui o projeto</p>
                    <span id="step-5-mobile-data-value" class="caption-15 font-weight-normal text-hex-797979 mb-0 float-left ml-2"></span>
                </div>
                <ion-icon name="chevron-down-outline"></ion-icon>
            </div>

            <div id="plan-select-area" class="animated fadeInLeft delay-600ms d-none d-md-block">
            </div>

        </div>
    </div>
    <div data-aos="fade" class="col-md-7  item-detail-col">
        <div onclick="openModalItemDetail('#modal_plan_detail')" id="current-plan-select-area"
            class="item-detail-img h-mob-64 mr-0 mr-md-4"
            style="background-image:  url('{{url('assets_front/imgs/detail-bg-plan.png')}}');">
            <p class="caption-14 text-middle-gray item-detail-detail mb-0 value-total-title">Valor parcial</p>
            <h4  class="ml-32 text-middle-gray  value-total-class font-weight-bold"></h4>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt="">
        </div>
    </div>
</div>

@push('modals')
<div class="modal fade p-0" id="modal_plan_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal fm-monte-o-seu" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button style="color: #fff; z-index: 90" type="button" class="close btn  p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon size="large" name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="zoom transition">
                    <img class="modal-item-img" id="modal_plan_detail-img" src="" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade pr-0" id="step-5-mobile-data" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog fullscreen-modal-white">
      <div class="modal-content">
        <div style="border-bottom: 0" class="modal-header">
          <button type="button" class="close mr-auto ml-0" data-dismiss="modal" aria-label="Close">
            <ion-icon size="large" name="chevron-down-outline"></ion-icon>
          </button>
        </div>
        <div class="modal-body">
            <div id="plan-select-area-mob" class="">
            </div>
        </div>
      </div>
    </div>
  </div>

@endpush
@push('scripts')
    <script>

        function selectPlan(name, id, price, planObject, obj) { 

            $('#disabled-plan-selec').removeAttr("hidden"); 
            if(obj){
                SetSelectedStep("5", obj);
            }

            //setting MOS data
            if(planObject.img){
                setItemMOS("#current-plan-select-area", "#modal_plan_detail-img", `{{url('storage/planned/${planObject.img}')}}`);
            }else {
                unsetItemMOS("#current-plan-select-area", "#modal_plan_detail-img", `{{url('assets_front/imgs/detail-bg-emp.png')}}`);
            }

            /* updateValueUnity(ap.price); */
            /* '{{url('storage/planned/${e.img}')}}' */
            console.log("showing the eletrodomestics render");
            console.log(oportunity);

            /* $("#collapse-electros-") */
            //planned_furniture
            /* if(id == oportunity.planned_furniture.id){

                oportunity.planned_furniture.price = "0";
                oportunity.planned_furniture.id = -1;
                oportunity.planned_furniture.selected = false;
                oportunity.planned_furniture.name = "Essa unidade não possui Móveis planejados selecionados";
                console.log(oportunity);
                $(oportunity.planned_furniture.htmlId).addClass('op-0');
                $(oportunity.planned_furniture.htmlIdMob).addClass('op-0');
                updateValueEmpreendimento();

                return;
            } */
            oportunity.planned_furniture.id = id;
            oportunity.planned_furniture.selected = true;
            oportunity.planned_furniture.name = name;
            console.log(oportunity);

            $(oportunity.planned_furniture.htmlId).addClass('op-0');
            $(oportunity.planned_furniture.htmlIdMob).addClass('op-0');
            /* console.log(oportunity.planned_furniture.htmlId); */
            /* console.log(id);
            console.log(price); */
            oportunity.planned_furniture.htmlId = '#check-selected-'+oportunity.planned_furniture.id;
            oportunity.planned_furniture.htmlIdMob = '#check-selected-'+oportunity.planned_furniture.id+'-mob';

            $(oportunity.planned_furniture.htmlId).removeClass('op-0');
            $(oportunity.planned_furniture.htmlIdMob).removeClass('op-0');
            /* console.log(oportunity.planned_furniture.htmlId); */


            oportunity.planned_furniture.price = ""+price;
            
            $("#step-5-mobile-data").modal('hide')
            $("#step-5-mobile-data-label").html(name);
            $("#step-5-mobile-data-label").removeClass("text-hex-797979");
            $("#step-5-mobile-data-label").addClass("text-middle-gray");

            var mobileValueFormatted = +price;
            $("#step-5-mobile-data-value").html("+ " + mobileValueFormatted.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}));

            updateValueEmpreendimento();
            //$('#disabled-plan-selec').removeAttr("disabled");

        }

    </script>
@endpush