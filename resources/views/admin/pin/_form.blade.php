<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('cat', 'Categoria', ['class' => '']) !!}
    {!! Form::text('cat', old('cat'), ['class' => 'form-control required']) !!}
    @if($errors->has('cat'))
      <span class="text-danger">{{ $errors->first('cat') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('slug', 'Slug', ['class' => '']) !!}
    {!! Form::text('slug', old('slug'), ['class' => 'form-control required']) !!}
    @if($errors->has('slug'))
      <span class="text-danger">{{ $errors->first('slug') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('img', 'Imagem da categoria', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img) <img src="{{url('storage/pin/'.$data->img)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img', old('img'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('color', 'Imagem Pin', ['class' => '']) !!}<br>
    @if(isset($data) && $data->color) <img src="{{url('storage/pin/'.$data->color)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('color', old('color'), ['class' => 'form-control']) !!}
  </div>
</div>

