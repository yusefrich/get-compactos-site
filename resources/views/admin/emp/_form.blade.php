@php 
  $img = isset($data) ? [] : []; 
@endphp
<div class="col-sm-12 col-md-12">
  <h2>Chamada</h2>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('ch_img', 'Imagem', ['class' => '']) !!}<br>
    @if(isset($data) && $data->ch_img) <img src="{{url('storage/emp/'.$data->ch_img)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('ch_img', $img) !!}
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('ch_title', 'Nome do empreendimento', ['class' => '']) !!}
    {!! Form::text('ch_title', old('ch_title'), ['class' => 'form-control']) !!}
    @if($errors->has('ch_title'))
    <span class="text-danger">{{ $errors->first('ch_title') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('ch_subtitle', 'Descrição', ['class' => '']) !!}
    {!! Form::text('ch_subtitle', old('ch_subtitle'), ['class' => 'form-control']) !!}
    @if($errors->has('ch_subtitle'))
    <span class="text-danger">{{ $errors->first('ch_subtitle') }}</span>
    @endif
  </div>
</div>
<br>
<div class="col-sm-12 col-md-12">
  <h2>Topo</h2>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('logo', 'Logo', ['class' => '']) !!}<br>
    @if(isset($data) && $data->logo) <img src="{{url('storage/emp/'.$data->logo)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('logo', $img) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('background', 'Background', ['class' => '']) !!}<br>
    @if(isset($data) && $data->background) <img src="{{url('storage/emp/'.$data->background)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('background', $img) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('head_title', 'Título', ['class' => '']) !!}
    {!! Form::text('head_title', old('head_title'), ['class' => 'form-control']) !!}
    @if($errors->has('head_title'))
    <span class="text-danger">{{ $errors->first('head_title') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('head_subtitle', 'Descrição', ['class' => '']) !!}
    {!! Form::textarea('head_subtitle', old('head_subtitle'), ['class' => 'form-control']) !!}
    @if($errors->has('head_subtitle'))
    <span class="text-danger">{{ $errors->first('head_subtitle') }}</span>
    @endif
  </div>
</div>
<br>
<div class="col-sm-12 col-md-12">
  <h2>Projeto</h2>
</div>
<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('title', 'Título', ['class' => '']) !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
    @if($errors->has('title'))
    <span class="text-danger">{{ $errors->first('title') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('status', 'Status', ['class' => '']) !!}
    {!! Form::select('status', [
    'Lançamento' => 'Lançamento',
    'Em construção' => 'Em andamento',
    'Pronto para morar' => 'Pronto para morar',
    'Portfólio' => 'Portfólio',
    ],old('status'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('img1', 'Imagem', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img1) <img src="{{url('storage/emp/'.$data->img1)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img1', $img) !!}
  </div>
</div>
<div class="col-sm-12 col-md-12">
  <div class="form-group mb-12">
    {!! Form::label('txt', 'Texto', ['class' => '']) !!}
    {!! Form::textarea('txt', old('txt'), ['class' => 'form-control']) !!}
    @if($errors->has('txt'))
    <span class="text-danger">{{ $errors->first('txt') }}</span>
    @endif
  </div>
</div>
<br>
<div class="col-sm-12 col-md-12">
  <h2>Ficha Técnica</h2>
</div>
<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('column1', 'Ficha técnica 1', ['class' => '']) !!}
    {!! Form::textarea('column1', old('column1'), ['class' => 'form-control']) !!}
    @if($errors->has('column1'))
    <span class="text-danger">{{ $errors->first('column1') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('column2', 'Ficha técnica 2', ['class' => '']) !!}
    {!! Form::textarea('column2', old('column2'), ['class' => 'form-control']) !!}
    @if($errors->has('column2'))
    <span class="text-danger">{{ $errors->first('column2') }}</span>
    @endif
  </div>
</div>
<br>
<div class="col-sm-12 col-md-12">
  <h2>Características</h2>
</div>

@include('admin.emp._icon', ['name' => 'icon1', 'txt' => 'Icone 1'])
<div class="col-sm-12 col-md-2">
  <div class="form-group mb-2">
    {!! Form::label('txt1', 'Texto 1', ['class' => '']) !!}
    {!! Form::text('txt1', old('txt1'), ['class' => 'form-control']) !!}
    @if($errors->has('txt1'))
    <span class="text-danger">{{ $errors->first('txt1') }}</span>
    @endif
  </div>
</div>

@include('admin.emp._icon', ['name' => 'icon2', 'txt' => 'Icone 2'])
<div class="col-sm-12 col-md-2">
  <div class="form-group mb-2">
    {!! Form::label('txt2', 'Texto 2', ['class' => '']) !!}
    {!! Form::text('txt2', old('txt2'), ['class' => 'form-control']) !!}
    @if($errors->has('txt2'))
    <span class="text-danger">{{ $errors->first('txt2') }}</span>
    @endif
  </div>
</div>

@include('admin.emp._icon', ['name' => 'icon3', 'txt' => 'Icone 3'])
<div class="col-sm-12 col-md-2">
  <div class="form-group mb-2">
    {!! Form::label('txt3', 'Texto 3', ['class' => '']) !!}
    {!! Form::text('txt3', old('txt3'), ['class' => 'form-control']) !!}
    @if($errors->has('txt3'))
    <span class="text-danger">{{ $errors->first('txt3') }}</span>
    @endif
  </div>
</div>
<div class="collapse multi-collapse col-12" id="collapse-1">
  <div class="row">
    @include('admin.emp._icon', ['name' => 'icon4', 'txt' => 'Icone 4'])

    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt4', 'Texto 4', ['class' => '']) !!}
        {!! Form::text('txt4', old('txt4'), ['class' => 'form-control']) !!}
        @if($errors->has('txt4'))
        <span class="text-danger">{{ $errors->first('txt4') }}</span>
        @endif
      </div>
    </div>
    @include('admin.emp._icon', ['name' => 'icon5', 'txt' => 'Icone 5'])

    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt5', 'Texto 5', ['class' => '']) !!}
        {!! Form::text('txt5', old('txt5'), ['class' => 'form-control']) !!}
        @if($errors->has('txt5'))
        <span class="text-danger">{{ $errors->first('txt5') }}</span>
        @endif
      </div>
    </div>
    @include('admin.emp._icon', ['name' => 'icon6', 'txt' => 'Icone 6'])

    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt6', 'Texto 6', ['class' => '']) !!}
        {!! Form::text('txt6', old('txt6'), ['class' => 'form-control']) !!}
        @if($errors->has('txt6'))
        <span class="text-danger">{{ $errors->first('txt6') }}</span>
        @endif
      </div>
    </div>
  </div>
</div>
<div class="collapse multi-collapse col-12" id="collapse-2">
  <div class="row">
    @include('admin.emp._icon', ['name' => 'icon7', 'txt' => 'Icone 7'])

    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt7', 'Texto 7', ['class' => '']) !!}
        {!! Form::text('txt7', old('txt7'), ['class' => 'form-control']) !!}
      </div>
    </div>
    @include('admin.emp._icon', ['name' => 'icon8', 'txt' => 'Icone 8'])
    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt8', 'Texto 8', ['class' => '']) !!}
        {!! Form::text('txt8', old('txt8'), ['class' => 'form-control']) !!}
      </div>
    </div>
    @include('admin.emp._icon', ['name' => 'icon9', 'txt' => 'Icone 9'])
    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt9', 'Texto 9', ['class' => '']) !!}
        {!! Form::text('txt9', old('txt9'), ['class' => 'form-control']) !!}
      </div>
    </div>
  </div>
</div>
<div class="collapse multi-collapse col-12" id="collapse-3">
  <div class="row">
    @include('admin.emp._icon', ['name' => 'icon10', 'txt' => 'Icone 10'])
    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt10', 'Texto 10', ['class' => '']) !!}
        {!! Form::text('txt10', old('txt10'), ['class' => 'form-control']) !!}
      </div>
    </div>
    @include('admin.emp._icon', ['name' => 'icon11', 'txt' => 'Icone 11'])
    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt11', 'Texto 11', ['class' => '']) !!}
        {!! Form::text('txt11', old('txt11'), ['class' => 'form-control']) !!}
      </div>
    </div>
    @include('admin.emp._icon', ['name' => 'icon12', 'txt' => 'Icone 12'])
    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt12', 'Texto 12', ['class' => '']) !!}
        {!! Form::text('txt12', old('txt12'), ['class' => 'form-control']) !!}
      </div>
    </div>
  </div>
</div>
<div class="collapse multi-collapse col-12" id="collapse-1">
  <div class="row">
    @include('admin.emp._icon', ['name' => 'icon13', 'txt' => 'Icone 13'])
    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt13', 'Texto 13', ['class' => '']) !!}
        {!! Form::text('txt13', old('txt13'), ['class' => 'form-control']) !!}
      </div>
    </div>
    @include('admin.emp._icon', ['name' => 'icon14', 'txt' => 'Icone 14'])
    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt14', 'Texto 14', ['class' => '']) !!}
        {!! Form::text('txt14', old('txt14'), ['class' => 'form-control']) !!}
      </div>
    </div>
    @include('admin.emp._icon', ['name' => 'icon15', 'txt' => 'Icone 15'])
    <div class="col-sm-12 col-md-2">
      <div class="form-group mb-2">
        {!! Form::label('txt15', 'Texto 15', ['class' => '']) !!}
        {!! Form::text('txt15', old('txt15'), ['class' => 'form-control']) !!}
      </div>
    </div>
  </div>
</div>
<div class="col-12 mb-3">
  <a class="btn btn-primary text-white" id="collapse-btn">{{-- data-toggle="collapse" 
    href="#multiCollapseExample1" 
    role="button" 
    aria-expanded="false" 
    aria-controls="multiCollapseExample1" --}}
    Adicionar mais
  </a>

</div>
<br>
<div class="col-sm-12 col-md-12">
  <h2>Book</h2>
</div>
<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('book', 'Book', ['class' => '']) !!}<br>
    {!! Form::file('book', old('book'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('imgbook', 'Imagem Book', ['class' => '']) !!}<br>
    @if(isset($data) && $data->imgbook) <img src="{{url('storage/emp/'.$data->imgbook)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('imgbook', old('imgbook'), ['class' => 'form-control']) !!}
  </div>
</div>
<br>
<div class="col-sm-12 col-md-12">
  <h2>Detalhes do andamento</h2>
</div>
<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('andtxt1', 'Item 1', ['class' => '']) !!}
    {!! Form::text('andtxt1', old('andtxt1'), ['class' => 'form-control']) !!}
    @if($errors->has('andtxt1'))
    <span class="text-danger">{{ $errors->first('andtxt1') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-1">
  <div class="form-group mb-1">
    {!! Form::label('andpct1', 'Pct 1', ['class' => '']) !!}
    {!! Form::number('andpct1', old('andpct1'), ['class' => 'form-control']) !!}
    @if($errors->has('andpct1'))
    <span class="text-danger">{{ $errors->first('andpct1') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('andtxt2', 'Item 2', ['class' => '']) !!}
    {!! Form::text('andtxt2', old('andtxt2'), ['class' => 'form-control']) !!}
    @if($errors->has('andtxt2'))
    <span class="text-danger">{{ $errors->first('andtxt2') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-1">
  <div class="form-group mb-1">
    {!! Form::label('andpct2', 'Pct 2', ['class' => '']) !!}
    {!! Form::number('andpct2', old('andpct2'), ['class' => 'form-control']) !!}
    @if($errors->has('andpct2'))
    <span class="text-danger">{{ $errors->first('andpct2') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('andtxt3', 'Item 3', ['class' => '']) !!}
    {!! Form::text('andtxt3', old('andtxt3'), ['class' => 'form-control']) !!}
    @if($errors->has('andtxt3'))
    <span class="text-danger">{{ $errors->first('andtxt3') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-1">
  <div class="form-group mb-1">
    {!! Form::label('andpct3', 'Pct 3', ['class' => '']) !!}
    {!! Form::number('andpct3', old('andpct3'), ['class' => 'form-control']) !!}
    @if($errors->has('andpct3'))
    <span class="text-danger">{{ $errors->first('andpct3') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('andtxt4', 'Item 4', ['class' => '']) !!}
    {!! Form::text('andtxt4', old('andtxt4'), ['class' => 'form-control']) !!}
    @if($errors->has('andtxt4'))
    <span class="text-danger">{{ $errors->first('andtxt4') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-1">
  <div class="form-group mb-1">
    {!! Form::label('andpct4', 'Pct 4', ['class' => '']) !!}
    {!! Form::number('andpct4', old('andpct4'), ['class' => 'form-control']) !!}
    @if($errors->has('andpct4'))
    <span class="text-danger">{{ $errors->first('andpct4') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('andtxt5', 'Item 5', ['class' => '']) !!}
    {!! Form::text('andtxt5', old('andtxt5'), ['class' => 'form-control']) !!}
    @if($errors->has('andtxt5'))
    <span class="text-danger">{{ $errors->first('andtxt5') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-1">
  <div class="form-group mb-1">
    {!! Form::label('andpct5', 'Pct 5', ['class' => '']) !!}
    {!! Form::number('andpct5', old('andpct5'), ['class' => 'form-control']) !!}
    @if($errors->has('andpct5'))
    <span class="text-danger">{{ $errors->first('andpct5') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('andtxt6', 'Item 6', ['class' => '']) !!}
    {!! Form::text('andtxt6', old('andtxt6'), ['class' => 'form-control']) !!}
    @if($errors->has('andtxt6'))
    <span class="text-danger">{{ $errors->first('andtxt6') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-1">
  <div class="form-group mb-1">
    {!! Form::label('andpct6', 'Pct 6', ['class' => '']) !!}
    {!! Form::number('andpct6', old('andpct6'), ['class' => 'form-control']) !!}
    @if($errors->has('andpct6'))
    <span class="text-danger">{{ $errors->first('andpct6') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('andtxt7', 'Item 7', ['class' => '']) !!}
    {!! Form::text('andtxt7', 'Projeto Total', ['class' => 'form-control']) !!}
    @if($errors->has('andtxt7'))
    <span class="text-danger">{{ $errors->first('andtxt7') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-1">
  <div class="form-group mb-1">
    {!! Form::label('andpct7', 'Pct 7', ['class' => '']) !!}
    {!! Form::number('andpct7', old('andpct7'), ['class' => 'form-control']) !!}
    @if($errors->has('andpct7'))
    <span class="text-danger">{{ $errors->first('andpct7') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('andent', 'Entrega Prevista', ['class' => '']) !!}
    {!! Form::date('andent', old('andent'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('andpr', 'Prazo Entrega', ['class' => '']) !!}
    {!! Form::date('andpr', old('andpr'), ['class' => 'form-control']) !!}
  </div>
</div>
<div class="col-sm-12 col-md-12">
  <h2>Localização</h2>
</div>
<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('lat', 'Latitude', ['class' => '']) !!}
    {!! Form::text('lat', old('lat'), ['class' => 'form-control']) !!}
  </div>
</div>
<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('long', 'Longitude', ['class' => '']) !!}
    {!! Form::text('long', old('long'), ['class' => 'form-control']) !!}
  </div>
</div>
<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('imgPin', 'Imagem Pin', ['class' => '']) !!}
    @if(isset($data) && $data->pin) <img src="{{url('storage/emp/'.$data->pin)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('imgPin', $img) !!}
  </div>
</div>
@section('scripts')
@include('admin.partials._maskmoney')
<script>
  var collapseItem = 1; 
    $( "#collapse-btn" ).click(function() {
      $('#collapse-' + collapseItem).collapse();
      collapseItem ++;
    });
    @if(isset($data) && $data->txt4)
      $("#collapse-btn").trigger('click');
    @endif
    @if(isset($data) && $data->txt7)
      $("#collapse-btn").trigger('click');
    @endif
    @if(isset($data) && $data->txt10)
      $("#collapse-btn").trigger('click');
    @endif
    @if(isset($data) && $data->txt13)
      $("#collapse-btn").trigger('click');
    @endif
</script>
@endsection
