<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('title', 'Títiulo', ['class' => '']) !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control required']) !!}
    @if($errors->has('title'))
      <span class="text-danger">{{ $errors->first('title') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('subtitle', 'Sub títiulo', ['class' => '']) !!}
    {!! Form::text('subtitle', old('subtitle'), ['class' => 'form-control required']) !!}
    @if($errors->has('subtitle'))
      <span class="text-danger">{{ $errors->first('subtitle') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('name', 'Autor', ['class' => '']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
    @if($errors->has('name'))
      <span class="text-danger">{{ $errors->first('name') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('img', 'Imagem', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img) <img src="{{url('storage/blog/'.$data->img)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img', old('img'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-12">
  <div class="form-group mb-12">
    {!! Form::label('txt', 'Imagem', ['class' => '']) !!}<br>
    {!! Form::textarea('txt', old('txt'), ['class' => 'form-control', 'id' => 'my-editor']) !!}
  </div>
</div>

@section('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
  };
  CKEDITOR.replace('my-editor', options);
</script>
@endsection
