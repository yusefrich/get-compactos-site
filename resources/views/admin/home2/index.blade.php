@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Home</h1>
      <div class="section-header-button mr-2">
        <a href="{{ route('admin.home.create') }}" class="btn btn-success btn-icon btn-lg btn-success" title="Adicionar"> <i class="fas fa-plus"></i> Adicionar</a>
      </div>
  </div>

  <div class="section-body">
    <div class="row mt-4">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>
              <i class="far fa-user lga"></i>
              Listagem<br>
              <small>{{ $data->total() }} resultados.</small>
            </h4>
          </div>
          <div class="card-body -table">
            <div class="table-responsive">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th>#</th>
                    <th>Título</th>
                    <th>IMG 1</th>
                    <th>IMG 2</th>
                    <th>Ações</th>
                  </tr>
                  @foreach($data as $d)
                    <tr>
                      <td>{{ $d->id }}</td>
                      <td>{{ $d->title }}</td>
                      <td>@if($d->img1) <img src="{{url('storage/home/'.$d->img1)}}" width="50px" style="margin-bottom: 5px;"> @endif</td>
                      <td>@if($d->img2) <img src="{{url('storage/home/'.$d->img2)}}" width="50px" style="margin-bottom: 5px;"> @endif</td>
                      {!! Form::open(['route' => ['admin.home.destroy', $d->id]]) !!}
                      <td class="no-wrap">
                        <a href="{{ route('admin.home.show', $d->id) }}" class="btn btn-success">Editar</a> | 
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Essa ação não pode ser desfeita!')">Apagar</button>
                      </td>
                      {!! Form::close() !!}
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer">
            <div class="float-right">
              <nav>
                {!! $data->render() !!}
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
