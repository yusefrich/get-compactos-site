<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('name', 'Local', ['class' => '']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control required']) !!}
    @if($errors->has('name'))
      <span class="text-danger">{{ $errors->first('name') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('lat', 'Latitude', ['class' => '']) !!}
    {!! Form::text('lat', old('lat'), ['class' => 'form-control required']) !!}
    @if($errors->has('lat'))
      <span class="text-danger">{{ $errors->first('lat') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('long', 'Logitude', ['class' => '']) !!}
    {!! Form::text('long', old('long'), ['class' => 'form-control required']) !!}
    @if($errors->has('long'))
      <span class="text-danger">{{ $errors->first('long') }}</span>
    @endif
  </div>
</div>

