<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand mb-2">
      <a href="{{ route('admin.panel') }}"><img src="{{ asset('/assets/img/brand.svg') }}" width="50"></a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('admin.panel') }}"><img src="{{ asset('/assets/img/brand.svg') }}" width="50"></a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">Módulos</li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown">
          <i class=" far fa-star lga"></i>
          <span>Home</span>
        </a>
        <ul class="dropdown-menu" style="display: none;">
          <li>
            <a class="nav-link" href="{{ route('admin.home.index') }}" title="Home">Home</a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('admin.feat.index') }}" title="Destaque">Destaques</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="{{ route('admin.about.admin') }}">
          <i class="far fa-star lga"></i>
          <span>Sobre</span>
        </a>
      </li>
      <li>
        <a href="{{ route('admin.pin.index') }}">
          <i class="far fa-star lga"></i>
          <span>Pins</span>
        </a>
      </li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown">
          <i class=" far fa-star lga"></i>
          <span>Empreendimentos</span>
        </a>
        <ul class="dropdown-menu" style="display: none;">
          <li>
            <a class="nav-link" href="{{ route('admin.emp.admin') }}" title="Home">Imóveis
            </a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('admin.electro.index') }}" title="Destaque">Eletrodomésticos</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="{{ route('admin.blog.index') }}">
          <i class="far fa-star lga"></i>
          <span>Conteúdos</span>
        </a>
      </li>
      <li>
        <a href="{{ route('admin.contact.admin') }}">
          <i class="far fa-star lga"></i>
          <span>Contatos</span>
        </a>
      </li>
    </ul>
  </aside>
</div>
