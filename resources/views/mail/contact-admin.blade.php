@component('mail::message')
<h4>Novo contato em {{config('app.name')}}</h4><br><br>

Nome: <b>{{$data->name}}</b><br>
Email: <b>{{$data->mail}}</b><br>
Telefone: <b>{{$data->phone}}</b><br>
Tipo: <b>{{$data->type}}</b><br>
@if($data->emp_id)
Empreendimento: <b>{{$data->emp->ch_title}}</b><br>
@endif
Mensagem: <b>{{$data->msg}}</b><br>

@endcomponent
