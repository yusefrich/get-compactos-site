<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home1 extends Model
{
    protected $table = 'home1';

    protected $fillable = [
        'title',
        'subtitle',
        'img1',
        'img2',
        'img3',
        'bt_title',
        'bt_url',
    ];
}
