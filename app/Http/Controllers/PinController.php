<?php

namespace App\Http\Controllers;

use App\PinCat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PinController extends Controller
{
    protected $pin;

    public function __construct(PinCat $pin) {
        $this->middleware('auth');
        $this->pin = $pin;
    }

    public function index()
    {
        $data = $this->pin->paginate();

        return view('admin.pin.index', compact('data'));
    }

    public function create()
    {        
        return view('admin.pin.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $imageName = time().'1.'.$input['img']->extension();
        $input['img']->move(public_path('storage/pin'), $imageName);
        $input['img'] = $imageName;

        $imageName = time().'2.'.$input['color']->extension();
        $input['color']->move(public_path('storage/pin'), $imageName);
        $input['color'] = $imageName;
        
        $this->pin->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.pin.index');
    }

    public function show($id)
    {
        $data = $this->pin->find($id);

        return view('admin.pin.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $imageName = time().'1.'.$input['img']->extension();
            $input['img']->move(public_path('storage/pin'), $imageName);
            $input['img'] = $imageName;
        } else {
            unset($input['img']);
        }

        if (isset($input['color'])) {
            $imageName = time().'2.'.$input['color']->extension();
            $input['color']->move(public_path('storage/pin'), $imageName);
            $input['color'] = $imageName;
        } else {
            unset($input['color']);
        }
        
        $this->pin->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.pin.index');
    }

    public function destroy($id)
    {
        $this->pin->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.pin.index');
    }
}
