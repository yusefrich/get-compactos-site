<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Emp;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class ConteudosController extends Controller
{
    use SEOToolsTrait;

    public function __construct() {
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $q = isset($input['q']) ? $input['q'] : 'all';
        $blog = Blog::take(6);
        if ($q == 'all' OR $q == 'recent') {
            $blog = $blog->orderBy('id', 'desc')->get();
        } else if($q == 'old') {
            $blog = $blog->orderBy('id', 'asc')->get();
        } else {
            $blog = $blog->where('title', 'LIKE', '%'.$input['q'].'%')->get();
        }
        
        $emp = Emp::inRandomOrder()->limit(3)->get();

        return view('pages.conteudos', compact('blog', 'emp', 'q'));
    }

    public function slug($slug)
    {
        $data = Blog::whereSlug($slug)->first();
        $blog = Blog::orderBy('id', 'desc')->take(4)->get();

        $this->seo()->setTitle($data->title);
        $this->seo()->setDescription($data->subtitle);

        $this->seo()->opengraph()->setDescription($data->subtitle);
        $this->seo()->opengraph()->setTitle($data->title);
        $this->seo()->opengraph()->setUrl(url('/conteudos/'.$data->slug));
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'pt-br');
        $this->seo()->opengraph()->addProperty('locale:alternate', ['pt-pt', 'en-us']);
        $this->seo()->opengraph()->addImage(url('/storage/blog/'.$data->img), ['height' => 451, 'width' => 800]);

        return view('pages.post', compact('data', 'blog'));
    }
}
