<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function __construct() {
        $this->middleware('guest');
    }

    public function contact(Request $request)
    {
        $messages = Contact::create($request->all()); 

        toastr()->success('Enviado. Logo entraremos em contato!');
        
        return back();
    }
}
