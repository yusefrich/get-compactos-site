<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpPhoto extends Model
{
    protected $table = 'emp_photos';
    
    protected $fillable = [
        'emp_id',
        'order',
        'type',
        'img',
        '1tt',
        '1stt',
        '2tt',
        '2stt',
        '3tt',
        '3stt',
        '4tt',
        '4stt',
        '5tt',
        '5stt',
        '6tt',
        '6stt',
    ];

    public function scopeOrder($query)
    {
        return $query->orderBy('order', 'asc');
    }
}
