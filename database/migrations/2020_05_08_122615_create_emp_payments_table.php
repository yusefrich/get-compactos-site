<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('emp_id');
            $table->string('title')->nullable();
            $table->string('desc')->nullable();
            $table->integer('entry')->nullable();
            $table->integer('semester')->nullable();
            $table->integer('qtd_semester')->nullable();
            $table->integer('montly')->nullable();
            $table->integer('qtd_montly')->nullable();
            $table->integer('financy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_payments');
    }
}
