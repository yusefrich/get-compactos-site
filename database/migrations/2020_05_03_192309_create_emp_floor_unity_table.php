<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpFloorUnityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_floor_unity', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('emp_id');
            $table->integer('floor_id');
            $table->string('name');
            $table->string('img');
            $table->decimal('price', 10, 2);
            $table->json('electros');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_floor_unity');
    }
}
