<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class JsonToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_floor_unity', function (Blueprint $table) {
            $table->text('electros')->change();
        });

        Schema::table('contact_simulations', function (Blueprint $table) {
            $table->text('floor')->change();
            $table->text('unity')->change();
            $table->text('project')->change();
            $table->text('electro')->change();
            $table->text('payment')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('string', function (Blueprint $table) {
            //
        });
    }
}
